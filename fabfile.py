from tools.fablib import *

from fabric.api import task


"""
Base configuration
"""
env.project_name = 'cornellsun'       # name for the project.
env.hosts = ['localhost', ]
env.sftp_deploy = True

# Environments
@task
def production():
    """
    Work on production environment
    """
    env.settings = 'production'
    env.hosts = [os.environ['CDS_PRODUCTION_SFTP_HOST'], ]
    env.user = os.environ['CDS_PRODUCTION_SFTP_USER']
    env.password = os.environ['CDS_PRODUCTION_SFTP_PASSWORD']
    env.domain = 'cornell.wpengine.com'
    env.port = 2222


@task
def staging():
    """
    Work on staging environment
    """
    env.settings = 'staging'
    env.hosts = [os.environ['CDS_STAGING_SFTP_HOST'], ]
    env.user = os.environ['CDS_STAGING_SFTP_USER']
    env.password = os.environ['CDS_STAGING_SFTP_PASSWORD']
    env.domain = 'cornell.wpengine.com'
    env.port = 2222

try:
    from local_fabfile import  *
except ImportError:
    pass